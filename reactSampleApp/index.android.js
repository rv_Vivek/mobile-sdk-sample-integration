import {AppRegistry} from 'react-native';
import app from './App';  

AppRegistry.registerComponent('appName', () => app);
export default app;