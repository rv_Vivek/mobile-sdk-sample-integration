package com.example.sample_project;
import android.content.Intent;
import android.os.Bundle;

import java.nio.ByteBuffer;
import android.content.Context;

import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.ActivityLifecycleListener;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import com.morfeus.android.websdk.core.MFSDKHeader;
import com.morfeus.android.websdk.core.MFSDKSessionProperties;
import com.morfeus.android.websdk.MFSDKMessagingManager;
import com.morfeus.android.websdk.MFSDKMessagingManagerKit;
import com.morfeus.android.websdk.MFSDKProperties;
import com.morfeus.android.websdk.MFSDKInitializationException;
import io.flutter.view.FlutterMain;

public class MainActivity extends FlutterActivity {
    private static MFSDKMessagingManager sMFSDK;

    private String sharedText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FlutterMain.startInitialization(this); //Added line
        super.onCreate(savedInstanceState);
        GeneratedPluginRegistrant.registerWith(this);
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        initMFSDKMessagingKit(this);
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendText(intent); // Handle text being sent
            }
        }

        new MethodChannel(getFlutterView(), "com.flutter.sample/sample").setMethodCallHandler(
                new MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall call, MethodChannel.Result result) {
                        if (call.method.contentEquals("Printy")) {
                            //result.success(sharedText);
                            //sharedText = null;
                            openChatActivity(getSessionProperties());
                        }
                    }
                });
    }
    private void openChatActivity(MFSDKSessionProperties sessionProperties) {
//        MyApplication.getMFSDK().showScreen(
//                MainActivity.this,
//                sessionProperties
//        );

        getMFSDK().showScreen(
                MainActivity.this,
                "94ha80984358821",
                sessionProperties);
    }
    private MFSDKSessionProperties getSessionProperties() {

        return new MFSDKSessionProperties
                .Builder()
                .disableUserInfoEncryption(true)

                .build();
    }
    private void initMFSDKMessagingKit(Context context) {
        MFSDKProperties sdkProperties = getMFSDKProperties();
        try {
            sMFSDK = new MFSDKMessagingManagerKit
                    .Builder(context)
                    .setSdkProperties(sdkProperties)
                    .build();

            sMFSDK.initWithProperties();
        } catch (MFSDKInitializationException e) {
        }
    }
    /**
     * Return an {@link MFSDKProperties} object that is pass to the {@link MFSDKMessagingManagerKit}
     *
     * @return MFSDKProperties
     */
    private MFSDKProperties getMFSDKProperties() {
        return new MFSDKProperties
                .Builder("https://tonikwebsdk-uat.active.ai")
                .addBot("94ha80984358821","FAB Mobile")
                .showNativeHeader(false)
                .build();
    }



    /**
     * Return an {@link MFSDKMessagingManagerKit} instance of MFSDKMessagingManagerKit
     *
     * @return sMFSDK is the static object of MFSDKMessagingManagerKit
     */
    public static MFSDKMessagingManager getMFSDK() {
        return sMFSDK;
    }

    void handleSendText(Intent intent) {
        sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
    }
}